import sys
import logging
import datetime
import pdb
from pymongo import MongoClient
from zmq_utils.mdwrkapi import MajorDomoWorker

def main():
    if len(sys.argv) < 3:
	sys.exit("Usage: %s host dbHost" % sys.argv[0])
    host = sys.argv[1]
    db_host = sys.argv[2]
    verbose = '-v' in sys.argv
    worker = MajorDomoWorker("tcp://%s:5555" % host, "last-data", verbose)
    reply = None

    try:
	client = MongoClient(db_host, 27017)
	db = client.byne_challenge
	posts = db.posts
	while True:
	    request = worker.recv(reply)
	    if request is None:
		break # Worker was interrupted

	    id, data = request[0].split(",")

	    result = posts.find({"author":id})

	    post = {"author": id, "data": data,
		    "date": datetime.datetime.utcnow()}

	    if result is None or result.count() == 0:
		res = posts.insert_one(post)
		reply = [str(data)]
	    else:
		if (int(data) > int(result[0]["data"])):
		    res = posts.update({"author": id}, post, upsert=True)
		    reply = [str(data)]
		else:
		    reply = [str(result[0]["data"])]

    except:
	logging.warning("Exception Caught")
    finally:
	client.close()
	return


if __name__ == '__main__':
    main()
