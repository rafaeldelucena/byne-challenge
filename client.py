import sys
import time
import logging
import random
from zmq_utils.mdcliapi2 import MajorDomoClient

def main():
    verbose = '-v' in sys.argv
    if len(sys.argv) < 3:
    	sys.exit("Usage: %s host clientId" % sys.argv[0])
    host = sys.argv[1]
    identity = sys.argv[2]
    client = MajorDomoClient("tcp://%s:5555" % host, verbose)
    last_data_timeout = 0.5
    last_data_time = time.time()
    numbers_time = time.time()
    total = 0
    counter = 1
    numbers_timeout = random.randint(30,50) / 10.0

    while True:
        try:
            if ((time.time() - last_data_time) > last_data_timeout):
                service = "last-data"
                client.send(service, "%s,%s" % (identity, total))
                reply = client.recv()
                if reply is None:
                    break
                logging.info("data received %s from %s" % (reply, service))
                total = int(reply[0]) + counter
                last_data_time = time.time()

            if ((time.time() - numbers_time) > numbers_timeout):
                value = random.randint(0,1)
                service = "even" if value == 0 else "odd"
                client.send(service, "")
                reply = client.recv()
                if reply is None:
                    break
                logging.info("data received %s from %s" % (reply, service))
                counter = int(reply[0])
                numbers_timeout = random.randint(30,50) / 10.0
                numbers_time = time.time()

        except KeyboardInterrupt:
            logging.warning("send interrupted, aborting")
            return

if __name__ == '__main__':
    main()
