# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###


O desafio constitui em arquitetar e desenvolver um sistema cliente/servidor, seguindo as seguintes diretivas:

- Servidor deve implementar dois serviços: Um que retorne números pares, e outro números ímpares;
- Cada cliente ao se conectar ao servidor deve iniciar um processo que incrementa o valor recebido do servidor em 1 a cada 500ms, enviando o novo valor ao servidor;
- Cada cliente deve, em um intervalo aleatório entre 3 e 5 segundos, requisitar ao servidor um número par ou ímpar, escolhido de forma aleatória, que será utilizado como novo valor de incremento, ao invés de 1;
- Os números devem estar sempre no range 0-99;
- Servidor deve enviar um valor ao aceitar conexão do cliente;
- Servidor deve manter o último valor enviado para cada cliente. Caso um mesmo cliente se conecte, enviar esse valor para o mesmo. Caso não haja valor registrado, enviar 0;
- Servidor deve manter um log de todas as mensagens trocadas;
- Deverá utilizar python2 e zeromq.

Também é importante lembrar que nossos produtos possuem foco em uso contínuo 24/7, com ciclo de vida esperado de 10 anos. Tolerância a falhas, redundância e simplicidade são essenciais.

### How do I get set up? ###
#### Project Dependencies ####
```
sudo apt-get install python-pip
sudo pip install pymongo
sudo pip install zmq
```
#### Getting and Running MongoDB ####
```
curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.4.2.tgz
tar -zxvf mongodb-linux-x86_64-3.4.2.tgz

./mongodb-linux-x86_64-3.4.2/bin/mongod --dbpath db_data &
```

#### Running Project ####
```
python zmq_utils/mdbroker.py &
python even-worker.py localhost &
python odd-worker.py localhost &
python last-data-worker.py localhost localhost &

for i in `seq 5`; do echo client$i; nohup python client.py localhost client$i &>> client$i.log & done
```
