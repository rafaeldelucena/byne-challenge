"""Majordomo Protocol worker example.

Uses the mdwrk API to hide all MDP aspects

Author: Min RK <benjaminrk@gmail.com>
"""

import sys
from random import randint
from zmq_utils.mdwrkapi import MajorDomoWorker

def main():
    if len(sys.argv) < 2:
    	sys.exit("Usage: %s host" % sys.argv[0])
    host = sys.argv[1]
    verbose = '-v' in sys.argv
    worker = MajorDomoWorker("tcp://%s:5555" % host, "even", verbose)
    reply = None
    while True:
        request = worker.recv(reply)
        if request is None:
            break # Worker was interrupted
        even = randint(0,49) * 2
        reply = [str(even)] 


if __name__ == '__main__':
    main()
